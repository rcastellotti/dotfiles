This dotfiles are based on [this](https://www.atlassian.com/git/tutorials/dotfiles) concept.

```bash
sudo apt install git neovim ranger zsh bat grc
```

```bash
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
git clone --bare https://gitlab.com/rcastellotti/dotfiles .dotfiles
``` 

 ```bash
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
git clone https://github.com/nojhan/liquidprompt .conf/zsh/liquidprompt
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git .conf/zsh/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions .conf/zsh/zsh-autosuggestions
config checkout
```

```bash
chsh -s /usr/bin/zsh
git config --global user.name "rcastellotti"
git config --global user.email "me@rcastellotti.dev"
```
